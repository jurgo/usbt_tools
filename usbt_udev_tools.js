var fs = require('fs');
var utils = require('utils');
var data = fs.read('conf/user.json', 'utf-8');
data = data.replace(/\n/g, '');
data = data.replace(/ /g, '');
data = data.replace(/'/g, '"');
var CONFIG = JSON.parse(data);


var casper = require('casper').create({
    verbose: false,
    logLevel: "debug"
});
casper.cli.drop("cli");
casper.cli.drop("casper-path");

if (casper.cli.args.length < 3 ) {
    casper.echo(casper.cli.args[0])    
    casper.echo("No arg nor option passed").exit();
}

var PARAMS = {
    project_name: casper.cli.args[0],
    branch_name: casper.cli.args[1],
    opts: casper.cli.args[2]
}
if(PARAMS.opts === 'commit'){
    PARAMS['commit_comment'] = casper.cli.args[3];
    PARAMS['commit_opts'] =  casper.cli.args[4];
}



var UDEV_URL = 'https://udev1a.net';

var usbt_steps = function(casper){
    this.login  = function(){
        var URL;

        casper.start('https://udev1a.net/', function login() {
            casper.fill('form[action="/web/login/j_spring_security_check"]', { 'j_username': CONFIG.login_username, 'j_password': CONFIG.login_password }, true);
        });//end start

        casper.thenOpen('https://udev1a.net/',  function(){
            url = 'https:'+this.evaluate(
                function(project_name) {
                    $('#teamPickerMenu span').click()
                    
                    var _prjBtn$ = $('.udCalloutScroll').has('.uddSel').find('li').filter( function(idx){  return $(this).text().replace(/[^a-zA-Z0-9]/g, "").toLowerCase() === project_name  } );
                    //if(_prjBtn$.length)
                      //  _prjBtn$.click();
                    var _url = $('#teamPicker option').eq($('.udCalloutScroll').has('.uddSel').find('li').index(_prjBtn$)).val()
                    return _url
                },{
                    'project_name': PARAMS.project_name
                }
            );
            this.thenOpen(url);
        })
    
        casper.then( function(){
           var url = this.evaluate(function(){
                return 'https://udev1a.net'+$('.workspaces').attr('href')
            });
            this.thenOpen(url); 
        });


        casper.page.paperSize = {
          width: '11in',
          height: '8.5in',
          orientation: 'landscape',
          border: '0.4in'
        };

        casper.on('remote.message', function printPopups(msg) {
            this.echo(msg);
        })

        casper.on('complete.error', function printErrors(err) {
            this.echo("Complete callback has failed: " + err);
            this.die("Complete callback has failed: " + err);
        });
    }//login

    this.goToBranch = function(){
        casper.thenEvaluate(
            function(branch_name){
                $('.udTile').filter( function(el){ return $(this).find('.name').text() == branch_name } ).click();
                $('.controlFwd:first').click();
            },{
               branch_name: PARAMS.branch_name  
            }
        );
    }


    this.showFiles = function(){
        this.goToBranch();
        casper.waitFor(function check() {
            return this.evaluate(function() {
                return (document.querySelectorAll('.udLiquidMain ul ').length > 0 )  || $('.udLiquidMain form').text() === "This workspace has no local changes." ;
            });
        }, function then() {
             this.evaluate(function showFiles2Commit(){
                //var filesText = $('.udLiquidMain').find('label, .uCell>li').get().map(function(el){ return ' '+el.textContent   }).join('');
                var filesText = $('.udLiquidMain').find('label, .uCell>li').get().map(function(el){ return ' '+el.textContent   })
                if(filesText.length){
                    console.log('file changed of modifiled:');
                    filesText.forEach(function(txt){
                        console.log(' - '+txt);
                    });                        
                }
                var _formText = $('.udLiquidMain form').text()
                if( _formText === "This workspace has no local changes.")
                 console.log(_formText);
            });
        }, function timeout(){
            this.echo("problem during show files")
        }


        );
    }

    this.commitFiles = function(){
       this.goToBranch();            
        casper.waitFor(function check() {
            return this.evaluate(function() {
                return document.querySelectorAll('.udLiquidMain ul').length > 0;
            });
        }, function then() {
             this.evaluate(function selectFilesToCommit(){
                    var filesModified$ = $('.udLiquidMain ul li').filter(':not(:has(li))');
                    console.log('File to commit: '+filesModified$.length)
                    filesModified$.each(function(idx){
                        console.log('- '+$(this).text())
                    });     
                    var sideLinks$ = $('.udLiquidSide>ul>li');
                    
                    //select H5
                    var sideLinksH5$ = sideLinks$.filter( function(){ return $(this).text().indexOf('front end') != -1  }  )
                    //var toogleH5$ = $('a.toggle');
                    //if(toogleH5$.length)
                        //toogleH5$.click();
                    //else{
                        sideLinksH5$.click();
                        $('.udLiquidMain .wsStatusBox>ul>li>input[type="checkbox"]').click();
                    //}
                    
                    //select WS
                    var sideLinksAWS$ = sideLinks$.filter( function(){ return $(this).text().indexOf('/site/v1') != -1  }  ); 
                    sideLinksAWS$.click();
                    $('.uCell li:not(.toggle)').click();
                    
                    //select MT
                    $('.udLiquidMain ul #mt0').click();
                    $('.uCell ul #mt0').click();
                });
        });

        casper.waitFor(function check() {
            return this.evaluate(function() {
                return $('.controlSave:not(.disabled)').length > 0;
            });
        }, function then() {
            this.evaluate(function openCommitPopup(){
                var commitBtn$ = $('.controlSave:not(.disabled)');
                commitBtn$.click();  
            });
        }, function timeout(){
            return this.evaluate(function() {
                console.log('Commit Button Not Actived!');
            });
        }

        );

        casper.waitFor(
            function check(commit_comment) {
                return this.evaluate(function() {
                    return $('#udMiniModal:visible').length > 0;
                });
            }, 
            function then() {
                    this.evaluate(function addCommitComment(commit_comment){
                        $('#udMiniModal:visible textarea').val(commit_comment);
                        $('#udMiniModal:visible textarea').change()
                    }, {'commit_comment': PARAMS['commit_comment']})
            }
        );

        casper.waitFor(function check() {
            return this.evaluate(function() {
                return $('#udMiniModal .controlSave:not(.disabled)').length > 0;
            });
        }, function then() {
            this.evaluate(function commitFiles(){
                $('#udMiniModal .controlSave:not(.disabled)').click()
            });
        });

        casper.waitFor(function check() {
            return this.evaluate(function() {
                return $('h1').filter(function(el){ return $(this).text() == 'commit successful' }  ).length > 0;
            });
        }, function then() {
            this.evaluate(function(){
                console.log($('h1').filter(function(el){ return $(this).text() == 'commit successful' }  ).text());
            });
        });
    }//end commitFiles()

    this.goToEnvironments = function(){
        casper.then(function(){
            var url = "https://udev1a.net"+this.evaluate(function(){
                return $('#udevelopMenu').find('a[href*="environments"]').attr('href');    
            });
            this.thenOpen(url);
        }); 
    }//end goToEnvironments
    this.goToEnv = function(envName){
        casper.then(
            function(){
                var url = this.evaluate(function(envName){
                     return $('#udevelopMenu .udevelopMenuBox a:contains("'+envName.toUpperCase()+'")').attr('href')
                 },{
                    'envName': envName
                });
                if(url)
                    this.thenOpen("https://udev1a.net"+url);
            }
        );
    } 

    this.syncEnv = function(envName){
        this.goToEnvironments();
        this.goToEnv(envName);
        casper.thenEvaluate(function selectBranchToUpdate(branch_name){
                console.log('win href: '+window.location.href)
                console.log('branch_name: '+branch_name)
                var branchBtn$ = $('.udTile').filter( function(el){ return $(this).find('.name').text() == branch_name } )
                console.log('branchBtn$.length: '+branchBtn$.length)
                branchBtn$.click()
                var syncBtn$ = $('.controlSync:first');
                console.log('syncBtn$.length: '+syncBtn$.length)
                syncBtn$.click()
            },
            PARAMS.branch_name
        )

        casper.waitFor(
            function check() {
                return this.evaluate(function() {
                    return $('#udMiniModal .controlSave:not(.disabled):visible').length > 0;
                });
            }, function then() {
                 this.evaluate(function(){
                    console.log('ENV UPDATED');
                    $('#udMiniModal .controlSave:not(.disabled):visible').click()
                });
            }, function timeout(){
                this.echo('env already synchronized')
                casper.exit();
            }, 10000
        );

        casper.waitFor(
            function check() {
                return this.evaluate(function() {
                    return $('#udMiniModalContent h1').filter(function(){ return $(this).text() == 'Environment synchronized'   }).length > 0;
                });
            },
            function then() {
                //this.echo('Close update popup');
                this.evaluate(function(){
                    console.log($('#udMiniModalContent p').text());
                    $('#udMiniModal .controlSave:not(.disabled):visible').click()
                });
            }, function timeout(){
                this.echo('timeout waiting update popup');
            }, 100000
        );
    }//end syncEnvs()


    this.updateEnv = function(){
        this.goToBranch();
        casper.waitFor(
            function check() {
                return this.evaluate(function() {
                    return $('h1').filter(function(){ return $(this).text().indexOf('workspace synchronized')  != -1}).length > 0;
                });
            }, 
            function then() {
                 this.evaluate(function selectFilesToCommit(){
                    console.log($('#udMiniModalContent h1').text())
                    console.log($('#udMiniModalContent p').text())   
                    $('#udMiniModalControls .controlSave').click()
                });
            },
            function timeout(){
                this.echo('env already synchronized')
                casper.exit();
            }, 8000
        );
    }//end updateEnv()

}//end usbt_steps

var usbt_steps = new usbt_steps(casper);



usbt_steps.login();
//usbt_steps.commitFiles();
//console.log('PARAMS.opts: '+PARAMS.opts)

if(PARAMS.opts === 'commit'){
    usbt_steps.commitFiles();
    if(PARAMS.commit_opts !== 'noupdate') 
        usbt_steps.syncEnv('qa');
}
if(PARAMS.opts === 'status')
    usbt_steps.showFiles()
if(PARAMS.opts === 'sync_qa')
    usbt_steps.syncEnv('qa');
if(PARAMS.opts === 'sync_uat')
   usbt_steps.syncEnv('uat');
if(PARAMS.opts === 'update_env')
    usbt_steps.updateEnv();




casper.run(function() {
    //this.capture('render.pdf');
    casper.exit();
});
