import Tkinter, ttk, subprocess, sys, time, os
from Tkinter import *
from subprocess import call

class App:
   def __init__(self, master):
      self.master = master
      self.windowTitle = StringVar()
      self.windowTitle.set('Usablenet Tools')
      self.master.title(self.windowTitle.get())

      # FRAMES
      self.headerFrame = Frame(master, bd=2)
      self.headerFrame.pack(side=TOP, padx=5, pady=5)

      self.commandsFrame = Frame(master)
      self.commandsFrame.pack(side=LEFT, padx=5, pady=5)

      self.historyFrame = Frame(master)
      self.historyFrame.pack(side=RIGHT, padx=5, pady=5)


      # BRANCHES DROPDOWN LIST
      self.branches = subprocess.Popen("usbt_projects_branch_list", shell=True, stdout=subprocess.PIPE).stdout.read()
      self.branches = self.branches.split('\n')

      self.currentBranch = StringVar()
      if((len(sys.argv) >= 2 ) and sys.argv[1]):
         self.currentBranch.set(sys.argv[1])
         self.windowTitle.set('Usablenet Tools - ' + self.currentBranch.get())
         self.master.title(self.windowTitle.get())

      
      self.boxValue = StringVar()
      self.branchesList = ttk.Combobox(self.headerFrame, textvariable=self.boxValue, state='readonly', width=30)
      self.branchesList['values'] = self.branches
      self.branchesList.bind('<<ComboboxSelected>>', self.selectBranch)
      if(self.currentBranch.get()):
         self.branchesList.set(self.currentBranch.get())
      else:
         self.branchesList.current(0)
         self.currentBranch.set(self.branchesList.get())
      self.branchesList.pack(side=LEFT)


      # PROJECT NAME
      # self.projectName = StringVar()
      # self.projectName.set(self.currentBranch)
      # self.projectNameField = Label(self.headerFrame, textvariable=self.currentBranch)
      # self.projectNameField.pack(side=LEFT)


      # COMMITS
      self.commitsFrame = Frame(self.commandsFrame)
      self.commitsFrame.pack(side=TOP)

      self.commitLabelsFrame = Frame(self.commitsFrame)
      self.commitLabelsFrame.pack(side=LEFT)

      self.commitFieldsFrame = Frame(self.commitsFrame)
      self.commitFieldsFrame.pack(side=LEFT)

      # TICKET NUMBER
      self.ticketNumberLabel = Label(self.commitLabelsFrame, text="Ticket Number (*)", width=25)
      self.ticketNumberValue = StringVar()
      self.ticketNumberField = Entry(self.commitFieldsFrame, textvariable=self.ticketNumberValue, width=50)
      self.ticketNumberLabel.pack(side=TOP)
      self.ticketNumberField.pack(side=TOP)

      # WORK COMMENT
      self.meaningfulCommentLabel = Label(self.commitLabelsFrame, text="Work comment (*)", width=25, justify=LEFT)
      self.meaningfulCommentValue = StringVar()
      self.meaningfulCommentField = Entry(self.commitFieldsFrame, textvariable=self.meaningfulCommentValue, width=50)
      self.meaningfulCommentLabel.pack(side=TOP)
      self.meaningfulCommentField.pack(side=TOP)

      # WORK LOG
      self.worklogLabel = Label(self.commitLabelsFrame, text="Work Log (*)", width=25)
      self.worklogValue = StringVar()
      self.worklogField = Entry(self.commitFieldsFrame, textvariable=self.worklogValue, width=50)
      self.worklogLabel.pack(side=TOP)
      self.worklogField.pack(side=TOP)

      # JIRA COMMENT OR ASSEGNEE
      self.jiraComment_or_email_Label = Label(self.commitLabelsFrame, text="Jira Comment / Assignee", width=25)
      self.jiraComment_or_email_Value = StringVar()
      self.jiraComment_or_email_Field = Entry(self.commitFieldsFrame, textvariable=self.jiraComment_or_email_Value, width=50)
      self.jiraComment_or_email_Label.pack(side=TOP)
      self.jiraComment_or_email_Field.pack(side=TOP)


      # ACTIONS
      self.commitActionsFrame = Frame(self.commandsFrame)
      self.commitActionsFrame.pack(side=TOP)

      branchStatusBtn = Tkinter.Button(self.commitActionsFrame, text="usbt_Commit", command=self.usbt_commit)
      branchStatusBtn.pack(side=LEFT)

      branchStatusBtn = Tkinter.Button(self.commitActionsFrame, text="(?)", command=self.commitHelper)
      branchStatusBtn.pack(side=LEFT)


      self.otherActionsFrame = Frame(self.commandsFrame)
      self.otherActionsFrame.pack(side=TOP)

      # BUTTON TO SHOW BRANCH STATUS
      branchStatusBtn = Tkinter.Button(self.otherActionsFrame, text="Branch Status", command=self.usbt_status)
      branchStatusBtn.pack(side=LEFT)
      # BUTTON TO UPDATE BRANCH
      branchStatusBtn = Tkinter.Button(self.otherActionsFrame, text="Update Branch", command=self.usbt_update)
      branchStatusBtn.pack(side=LEFT)
      # BUTTON TO SYNC QA
      branchStatusBtn = Tkinter.Button(self.otherActionsFrame, text="Sync QA", command=self.usbt_sync_qa)
      branchStatusBtn.pack(side=LEFT)
      # BUTTON TO SYNC UAT
      branchStatusBtn = Tkinter.Button(self.otherActionsFrame, text="Sync UAT", command=self.usbt_sync_uat)
      branchStatusBtn.pack(side=LEFT)


      # APP MESSAGES
      self.message = StringVar()
      self.appMessages = Label(self.commandsFrame, textvariable=self.message, anchor=W, justify=LEFT, pady=10)
      self.appMessages.pack(side=TOP)

      
      self.appActionsFrame = Frame(self.commandsFrame)
      self.appActionsFrame.pack(side=TOP)

      # BUTTON TO UPDATE THE APP
      versionUpdateBtn = Tkinter.Button(self.appActionsFrame, text="Check for new version", command=self.usbt_utils_versionupdate)
      versionUpdateBtn.pack(side=RIGHT)
      # BUTTON TO CLEAR MESSAGES
      clearMessagesBtn = Tkinter.Button(self.appActionsFrame, text="Clear messages", command=self.clear_mex)
      clearMessagesBtn.pack(side=RIGHT)


   # CALLBACKS
   def selectBranch(self, event):
         self.currentBranch.set(event.widget.get())
         self.message.set('')
         self.windowTitle.set('Usablenet Tools - ' + '/'.join(self.currentBranch.get().split('/')[-2:]))
         self.master.title(self.windowTitle.get())

   def usbt_commit(self):
         a = self.meaningfulCommentValue.get()
         b = self.ticketNumberValue.get()
         c = self.worklogValue.get()
         d = self.jiraComment_or_email_Value.get()
         qt = '"'
         commitArgs = [qt+str(a)+qt, qt+str(b)+qt,qt+str(c)+qt,'']
         if d:
            commitArgs[3] = qt+str(d)+qt
         commitArgs = '%s %s %s %s' % tuple(commitArgs)
         self.executeCommand('usbt_commit '+ commitArgs)

   def commitHelper(self):
         self.executeCommand('usbt_commit -h')

   def usbt_status(self):
         self.executeCommand('usbt_status')

   def usbt_update(self):
         self.executeCommand('usbt_update')

   def usbt_sync_qa(self):
         self.executeCommand('usbt_sync_qa')

   def usbt_sync_uat(self):
         self.executeCommand('usbt_sync_uat')


   def clear_mex(self):
         self.message.set('')

   def usbt_utils_versionupdate(self):
         self.executeCommand('usbt_utils_versionupdate')


   # UTILITY FUNCTIONS
   def executeCommand(self, command):
         new_message = subprocess.Popen("cd "+ self.currentBranch.get() +" && "+ command, shell=True, stdout=subprocess.PIPE).stdout.read()
         self.message.set(new_message)
         self.notification(self.message.get())

   def raiseAbove(self):
      # TO OPEN WINDOW ON TOP
      os.system('''/usr/bin/osascript -e 'tell app "Finder" to set frontmost of process "Python" to true' ''')

   def notification(self, message):
      os.system('''/usr/bin/osascript -e 'display notification "''' + message + '''" with title "Usablenet Tools"' ''')


if __name__ == '__main__':
   root = Tkinter.Tk()
   app = App(root)
   app.raiseAbove()
   root.mainloop()
   
   
   
