//var request = require('request');
var https = require('https');
var fs = require('fs');
//var dataUser = fs.read('conf/user.json', 'utf-8');
var dataUser = fs.readFileSync('conf/user.json', 'utf8');
dataUser = dataUser.replace(/'/g, '"');
dataUser = JSON.parse(dataUser)
var jira_host = "jira.usablenet.com";
var jira_api_path = "/rest/api/latest/";
var jira_api_url = 'https://'+jira_host+jira_api_path;

var auth = "Basic " + new Buffer(dataUser.login_username + ":" + dataUser.jira_password).toString("base64");
var args = process.argv.slice(2);

var _comment_visible = null;
var _email_2assign = null;
var mailOrComment = args[3];
if(mailOrComment && mailOrComment.match(/^[a-z\.]*@usablenet.com$/) )
    _email_2assign = mailOrComment;
if(mailOrComment && !mailOrComment.match(/^[a-z\.]*@usablenet.com$/) )
    _comment_visible = mailOrComment;

var PARAMS = {
    'ticket' : args[0],
    'comment': args[1],
    'timeSpent': args[2],
    'comment_visible': _comment_visible,
    'mail_assign': _email_2assign

}


//var ticket = "JIRAPI-22"
//var ticket = "BEHRMM-683"
//var ticketPath = jira_api_path+"issue/"+ticket+"?expand=renderedFields,names,schema,transitions,operations,editmeta,changelog"

//ADD WORKLOG
var ticketPathWorklog = jira_api_path+"issue/"+PARAMS.ticket+"/worklog?adjustEstimate=AUTO"
var updateTicketDataWorklog= {
	"comment":PARAMS.comment,
	"timeSpent":PARAMS.timeSpent
}
updateTicket(auth, updateTicketDataWorklog, ticketPathWorklog);


if(PARAMS.comment_visible){
    console.log('PARAMS.comment_visible: '+PARAMS.comment_visible)
    //CHANGE STATUS (Ready on QA)
    var ticketPathChangeStatus = jira_api_path+"issue/"+PARAMS.ticket+"/transitions?expand=transitions.fields"
    var updateTicketDataChangeStatus = {
        "update": {
            "comment": [
                {
                    "add": {
                        "body": PARAMS.comment_visible
                    }
                }
            ]
        },
        "transition": {
            "id": "31"
        }
    }
    updateTicket(auth, updateTicketDataChangeStatus, ticketPathChangeStatus);


    var ticketPathAssign = jira_api_path+"issue/"+PARAMS.ticket+'/assignee';
    var assignData= {
        "name":'-1'
    }
    updateTicket(auth, assignData, ticketPathAssign, 'PUT');


}

if(PARAMS.mail_assign){
    console.log('PARAMS.mail_assign: '+PARAMS.mail_assign)
    var ticketPathAssign = jira_api_path+"issue/"+PARAMS.ticket+'/assignee';
    var assignData= {
        "name":PARAMS.mail_assign
    }
    updateTicket(auth, assignData, ticketPathAssign, 'PUT');
}

function updateTicket(auth, updateTicketData, ticketPath, method){
    var _method = method ? method : 'POST'
    console.log('_method: '+_method)
    console.log('ticketPath: '+ticketPath)
    updateTicketData = JSON.stringify(updateTicketData);
    var headers = {
        "Authorization" : auth,
        'Content-Type': 'application/json',
        'Content-Length': updateTicketData.length
    }
    var optionspost = {
        host : jira_host,
        port : 443,
        path : ticketPath,
        method : _method,
        headers : headers
    };
    var responseString = '';
    var reqPost = https.request(optionspost, function(res) {
        console.log("statusCode: ", res.statusCode); 
        res.on('data', function(d) {
            responseString += d;
        });
            res.on('end', function() {
                //console.log('end - '+ticketPath)
                //process.stdout.write(responseString);
                //console.info('\n\n' +_method+' ended');
            });

        })
        reqPost.write(updateTicketData);
        reqPost.end();
        reqPost.on('error', function(e) {
            console.error(e);
    });
}