#WHAT IS THIS?#

usbt tools is a group of bash tools who needs to make the usbt developer life easier.
There's also a pretty stand-alone gui who can be called easily from TextMate.

![Screen Shot 2014-10-23 at 12.56.29.png](https://bitbucket.org/repo/KGejqR/images/547602992-Screen%20Shot%202014-10-23%20at%2012.56.29.png)


# README #


### How to install ###
Every command should be cuttend and pasted one row per-time in your terminal


be sure to have installed node.js in your computer. 
You can download it from http://nodejs.org/

now you should install the needed library:

if you **are not** a sudoer (if you have an **iMac**, probably this is your case), plese log as an sudoer user.
e.g if your user is "Administrator", do:
```
#!bash
su - Administrator
```
then install the needed library (we need to install phantomjs and casperjs:
```
#!bash
sudo npm install phantomjs -g
sudo npm install casperjs -g
```

if you have changed the user becouse your user is not a sudoers, then logout from sudouser (Administrator in my example) in order to the your normal user:
```
#!bash
exit
```

Now just exec this last row to install the usbt_tools:
```
#!bash
curl -O https://bitbucket.org/jurgo/usbt_tools/raw/master/install.sh &&  chmod +x install.sh && ./install.sh
```

now you have to configure your tool inserting your credendial in conf/user.json file, you open this with an editor or run:

```
#!bash
cd
cd usbt_tools
open conf/user.json 
```


### user interface ###

you can call usbt_tools_gui inside your project directory in order to launch the gui interface


### use it with sublime ###
on sublime, you can add a build system, so when you will click CMD + B the gui will be opened:

to create the build:
**go to tools -> Build System -> New Build system**

if you have Sublime Text 3, put this:

```
#!javascript
{
	"shell_cmd": "usbt_tools_gui", 
	"working_dir": "${file_path:${folder}}",
}
```
if you have Sublime Text 2 put this:


```
#!javascript
{
	"cmd": "usbt_tools_gui", 
	"working_dir": "${file_path:${folder}}",
	"shell": "bash",
	"env": {"PATH": "$PATH:~/usbt_tools"}
}
```

then enjoy