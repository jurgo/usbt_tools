#!/bin/bash
cd "/tmp/" 
[ -z $(which node) ] && curl http://nodejs.org/dist/v0.10.31/node-v0.10.31.pkg -o nodejs.pkg && open nodejs.pkg

cd ~
[ -d usbt_tools ] && rm -Rf usbt_tools
mkdir usbt_tools
git clone https://bitbucket.org/jurgo/usbt_tools/ usbt_tools

echo "insert your password: "
if [ -z "$((sudo -v) 2>&1 )" ]; then
	echo "-- you are a sudoers"
	[ -z "$(which phantomjs)" ] && sudo  npm install -g phantomjs
	[ -z "$(which casperjs)" ] && sudo  npm install -g casperjs
else
	echo "-- you are NOT a sudoers"
	echo "insert Administrator password: "
    [ -z "$(which phantomjs)" ] && su - Administrator -c 'npm install -g phantomjs'
	[ -z "$(which casperjs)" ] && su - Administrator -c 'npm install -g casperjs'
fi




cd usbt_tools
npm install
cd ..

[ -z "$(cat ~/.bash_profile |grep ":~/usbt_tools")" ] && echo -e  'export PATH=$PATH:~/usbt_tools'"\n $(cat ~/.bash_profile)" > ~/.bash_profile
exec bash -l
