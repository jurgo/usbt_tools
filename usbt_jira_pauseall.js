//var request = require('request');
var https = require('https');
var fs = require('fs');
var request = require('request');
//var dataUser = fs.read('conf/user.json', 'utf-8');
var dataUser = fs.readFileSync('conf/user.json', 'utf8');
dataUser = dataUser.replace(/'/g, '"');
dataUser = JSON.parse(dataUser)

var jira_host = "jira.usablenet.com";
var jira_api_path = "/rest/api/latest/";
var jira_api_url = 'https://'+jira_host+jira_api_path;

var auth = "Basic " + new Buffer(dataUser.login_username + ":" + dataUser.jira_password).toString("base64");
//var args = process.argv.slice(2);


var jqlString = 'assignee = "'+dataUser.login_username+'" and status = "In Progress"'



var url = jira_api_url+"search?jql="+encodeURI(jqlString)+"&maxResults=1000"

var headers = {"Authorization" : auth , 'Content-Type': 'application/json' }


request(
	{url: url, headers: headers}, 
	function(error, response, responseBody){
        if(!error){
        	console.log('<- retrived main url: '+url);

        	responseBody = JSON.parse(responseBody);

        	responseBody.issues.forEach(function(ticket){
        		console.log('key: '+ticket.key);
			    
        		var transitionId = "371";
        		if(ticket.fields.issuetype.id === "16")
        			transitionId = "451"

			    
			    var pauseTicket = changeStatusData(transitionId)
			    var ticketPathChangeStatus = jira_api_path+"issue/"+ticket.key+"/transitions?expand=transitions.fields"
        		updateTicket(auth, pauseTicket, ticketPathChangeStatus);
        		
        	});





        	//console.log('responseBody: '+responseBody)
		}else{
			console.log('request error: '+error);
		}
	}
);//end request	


function changeStatusData(id){
	return {
        "transition": {
            "id": id
        }
    }

}


function updateTicket(auth, updateTicketData, ticketPath, method){
    var _method = method ? method : 'POST'
    updateTicketData = JSON.stringify(updateTicketData);
    var headers = {
        "Authorization" : auth,
        'Content-Type': 'application/json',
        'Content-Length': updateTicketData.length
    }
    var optionspost = {
        host : jira_host,
        port : 443,
        path : ticketPath,
        method : _method,
        headers : headers
    };
    var responseString = '';
    var reqPost = https.request(optionspost, function(res) {
        //console.log("statusCode: ", res.statusCode); 
        res.on('data', function(d) {
            responseString += d;
        });
        res.on('end', function() {
        	console.log('Ticket Paused: '+ticketPath.match(/issue\/(.*)\/transitions/)[1])
        	// var _res = JSON.parse(responseString);
        	// if(_res.errorMessages){
        	// 	console.log('_res.errorMessages: '+_res.errorMessages)
        	// 	//updateTicket(auth, pauseTicket, ticketPathChangeStatus);
        	// }

            //console.log('end - '+ticketPath)
            //process.stdout.write(responseString);
            //console.info('\n\n' +_method+' ended');
        });

        })
        reqPost.write(updateTicketData);
        reqPost.end();
        reqPost.on('error', function(e) {
            console.error('error requeat: '+e);

            //responseString
            //return   errorMessages
    });
}	
